/*!
 * \file common.h
 * \author Шиганцов А.А.
 * \brief Заголовочный файл кодирования NAVDAT
 */

#ifndef __NAVDAT_ENCODING_H__
#define __NAVDAT_ENCODING_H__

#include "common.h"
#include "../Multiplex/common.h"
#include <liquid/liquid.h>

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
  NAVDAT_TIS_qamMode_t tisMode;
  NAVDAT_DS_qamMode_t dsMode;

  NAVDAT_xIS_Code_t xisCode;
  NAVDAT_DS_Code_t dsCode;
} NAVDAT_CodecCfg_t;

typedef struct
{
  NAVDAT_TIS_qamMode_t tisMode;
  NAVDAT_DS_qamMode_t dsMode;

  NAVDAT_xIS_Code_t xisCode;
  NAVDAT_DS_Code_t dsCode;

  packetizer misPack, tisPack, dsPack;
  Bool_t NeedReinitTIS, NeedReinitDS;
} NAVDAT_Codec_t;

INLINE void NAVDAT_Codec_setTisMode(NAVDAT_Codec_t* This, NAVDAT_TIS_qamMode_t mode){if(This->tisMode != mode){This->tisMode = mode;This->NeedReinitTIS=1;}}
INLINE void NAVDAT_Codec_setDsMode(NAVDAT_Codec_t* This, NAVDAT_DS_qamMode_t mode){if(This->dsMode != mode){This->dsMode = mode;This->NeedReinitDS=1;}}
INLINE void NAVDAT_Codec_setDsCode(NAVDAT_Codec_t* This, NAVDAT_DS_Code_t code){if(This->dsCode != code){This->dsCode = code;This->NeedReinitDS=1;}}

void NAVDAT_Codec_reinitTIS(NAVDAT_Codec_t* This);
void NAVDAT_Codec_reinitDS(NAVDAT_Codec_t* This);

void init_NAVDAT_Codec(NAVDAT_Codec_t* This, NAVDAT_CodecCfg_t* Cfg);
void navdat_encode(NAVDAT_Codec_t* This, NAVDAT_RawFrame_t* inputFrame, NAVDAT_RawFrame_t* outputFrame);
Bool_t navdat_decode_mis(NAVDAT_Codec_t* This, NAVDAT_RawFrame_t* inputFrame, NAVDAT_RawFrame_t* outputFrame);
Bool_t navdat_decode_tis(NAVDAT_Codec_t* This, NAVDAT_RawFrame_t* inputFrame, NAVDAT_RawFrame_t* outputFrame);
Bool_t navdat_decode_ds(NAVDAT_Codec_t* This, NAVDAT_RawFrame_t* inputFrame, NAVDAT_RawFrame_t* outputFrame);
void deinit_NAVDAT_Codec(NAVDAT_Codec_t* This);
#ifdef __cplusplus
}
#endif

#endif // __NAVDAT_ENCODING_H__
