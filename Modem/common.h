/*!
 * \file common.h
 * \author Шиганцов А.А.
 * \brief Общие елементы для модулчции NAVDAT
 */

#ifndef __NAVDAT_MODULATION_H__
#define __NAVDAT_MODULATION_H__

#include "SDR/BASE/common.h"
#include "SDR/BASE/Abstract.h"
#include "SDR/BASE/Modulation/digital/qamModulation.h"

#ifdef __cplusplus
extern "C"{
#endif

#define NAVDAT_MODE 'B'

#define NAVDAT_Tu_ms ((float)256/12)
#define NAVDAT_TuPerTg 4
#define NAVDAT_Tg_ms (NAVDAT_Tu_ms/NAVDAT_TuPerTg)
#define NAVDAT_Ts_ms (NAVDAT_Tu_ms + NAVDAT_Tg_ms)

#define NAVDAT_Nfft(fftSizeFactor) (fftSizeFactor*256)

#define NAVDAT_OFDM_GUARD_INTERVAL_SAMPLES_COUNT(fftSizeFactor) ((Size_t)(NAVDAT_Tg_ms*NAVDAT_Nfft(fftSizeFactor)/NAVDAT_Tu_ms))
#define NAVDAT_OFDM_SYMBOL_SAMPLES_COUNT(fftSizeFactor) ((Size_t)(NAVDAT_Nfft(fftSizeFactor) + NAVDAT_OFDM_GUARD_INTERVAL_SAMPLES_COUNT(fftSizeFactor)))

#define NAVDAT_FRAME_ofdmSYMBOLS_COUNT 15
#define NAVDAT_GAIN_PILOTS_PERIOD 3

#define NAVDAT_A0 0.707106781188
#define NAVDAT_A_qam16 (0.3162277660168379332*NAVDAT_A0)
#define NAVDAT_A_qam64 (0.15430334996209191026*NAVDAT_A0)

INLINE Sample_t NAVDAT_aFactor(Int8_t qamSymCount)
{
  switch(qamSymCount)
  {
  case 4:
    return NAVDAT_A0;
  case 16:
    return NAVDAT_A_qam16;
  case 64:
    return NAVDAT_A_qam64;
  default:
    SDR_ASSERT(0);
    return 0;
  }
}

#include "dataCount.h"

/* Режимы QAM. Значение соответствует количеству бит в символе. */
typedef enum {NAVDAT_TIS_qam4 = 2, NAVDAT_TIS_qam16 = 4} NAVDAT_TIS_qamMode_t;
typedef enum {NAVDAT_DS_qam4 = 2, NAVDAT_DS_qam16 = 4, NAVDAT_DS_qam64 = 6} NAVDAT_DS_qamMode_t;

#ifdef NAVDAT_DEBUG_MODULATION_PILOTS_PHASE_CONST
#define NAVDAT_PILOT_PH {1.4, 0}
#endif

typedef struct
{
  NAVDAT_TIS_qamMode_t tisMode;
  NAVDAT_DS_qamMode_t dsMode;

  qamBuffer_t mis, tis, ds;
} NAVDAT_qamData_t;

typedef struct
{
  Size_t mis, tis, ds;
} NAVDAT_DataCounter_t;

INLINE void NAVDAT_DataCounter_reset(NAVDAT_DataCounter_t* p)
{
  p->mis = 0;
  p->tis = 0;
  p->ds = 0;
}

#ifdef __cplusplus
}
#endif

#endif // __NAVDAT_MODULATION_H__
