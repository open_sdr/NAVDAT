/*!
 * \file qamConstellations.c
 * \author Шианцов А.А.
 * \brief QAM созвездия NAVDAT
 */

#include "SDR/BASE/Modulation/digital/qamModulation.h"

static iqSample_t drmQAM4[4] = {{ 1, 1},{-1, 1},
                                { 1,-1},{-1,-1}};

static iqSample_t drmQAM16[16] = {{ 3, 3},{ 1, 3},{-1, 3},{-3, 3},
                                  { 3, 1},{ 1, 1},{-1, 1},{-3, 1},
                                  { 3,-1},{ 1,-1},{-1,-1},{-3,-1},
                                  { 3,-3},{ 1,-3},{-1,-3},{-3,-3}};

static iqSample_t drmQAM64[64] = {{ 7, 7},{ 5, 7},{ 3, 7},{ 1, 7},{-1, 7},{-3, 7},{-5, 7},{-7, 7},
                                  { 7, 5},{ 5, 5},{ 3, 5},{ 1, 5},{-1, 5},{-3, 5},{-5, 5},{-7, 5},
                                  { 7, 3},{ 5, 3},{ 3, 3},{ 1, 3},{-1, 3},{-3, 3},{-5, 3},{-7, 3},
                                  { 7, 1},{ 5, 1},{ 3, 1},{ 1, 1},{-1, 1},{-3, 1},{-5, 1},{-7, 1},
                                  { 7,-1},{ 5,-1},{ 3,-1},{ 1,-1},{-1,-1},{-3,-1},{-5,-1},{-7,-1},
                                  { 7,-3},{ 5,-3},{ 3,-3},{ 1,-3},{-1,-3},{-3,-3},{-5,-3},{-7,-3},
                                  { 7,-5},{ 5,-5},{ 3,-5},{ 1,-5},{-1,-5},{-3,-5},{-5,-5},{-7,-5},
                                  { 7,-7},{ 5,-7},{ 3,-7},{ 1,-7},{-1,-7},{-3,-7},{-5,-7},{-7,-7}};

QAM_Constellation_t NAVDAT_DRM_QAM4_Constellation = {4,drmQAM4};
QAM_Constellation_t NAVDAT_DRM_QAM16_Constellation = {16,drmQAM16};
QAM_Constellation_t NAVDAT_DRM_QAM64_Constellation = {64,drmQAM64};
