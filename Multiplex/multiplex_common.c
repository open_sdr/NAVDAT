/*!
 * \file common.h
 * \author Шиганцов А.А.
 * \brief Реализация общих функций мультиплекса NAVDAT
 */

#include "common.h"
#include <string.h>
#include <stdlib.h>

#include "SDR/BASE/Multiplex/symMultiplex.h"

static Bool_t FrameToRawData_mis(NAVDAT_RawFrame_t *Frame, NAVDAT_qamData_t *Data)
{
  symMultiplexCfg_t cfg;
  cfg.symSize = 2;
  cfg.Buf.P = (UInt8_t*)Data->mis.P;
  cfg.Buf.Size = Data->mis.Size;

  symMultiplex_t mux;
  init_symMultiplex(&mux, &cfg);

  Bool_t res = sym_multiplex_write_buf(&mux, Frame->misData, Frame->Count.mis);
  Data->mis.Size = symMultiplex_Count(&mux);
  return res;
}

static Bool_t FrameToRawData_tis(NAVDAT_RawFrame_t *Frame, NAVDAT_qamData_t *Data)
{
  symMultiplexCfg_t cfg;
  cfg.symSize = Frame->tisMode;
  cfg.Buf.P = (UInt8_t*)Data->tis.P;
  cfg.Buf.Size = Data->tis.Size;

  symMultiplex_t mux;
  init_symMultiplex(&mux, &cfg);

  Bool_t res = sym_multiplex_write_buf(&mux, Frame->tisData, Frame->Count.tis);
  Data->tis.Size = symMultiplex_Count(&mux);
  return res;
}

static Bool_t FrameToRawData_ds(NAVDAT_RawFrame_t *Frame, NAVDAT_qamData_t *Data)
{
  symMultiplexCfg_t cfg;
  cfg.symSize = Frame->dsMode;
  cfg.Buf.P = (UInt8_t*)Data->ds.P;
  cfg.Buf.Size = Data->ds.Size;

  symMultiplex_t mux;
  init_symMultiplex(&mux, &cfg);

  Bool_t res = sym_multiplex_write_buf(&mux, Frame->dsData, Frame->Count.ds);
  Data->ds.Size = symMultiplex_Count(&mux);
  return res;
}

Bool_t NAVDAT_FrameToData(NAVDAT_RawFrame_t *Frame, NAVDAT_qamData_t *Data)
{
  Bool_t res = 0;

  res |= FrameToRawData_mis(Frame, Data);
  res |= FrameToRawData_tis(Frame, Data);
  res |= FrameToRawData_ds(Frame, Data);

  Data->tisMode = Frame->tisMode;
  Data->dsMode = Frame->dsMode;

  return res;
}

Bool_t NAVDAT_misBufToFrame(qamBuffer_t *Data, NAVDAT_RawFrame_t *Frame)
{
  symMultiplexCfg_t cfg;
  cfg.symSize = 2;
  cfg.Buf.P = (UInt8_t*)Data->P;
  cfg.Buf.Size = Data->Size;

  symMultiplex_t mux;
  init_symMultiplex(&mux, &cfg);

  Frame->Count.mis = NAVDAT_misRawCountMax();
  return sym_multiplex_read_buf(&mux, Frame->misData, Frame->Count.mis);
}

Bool_t NAVDAT_tisBufToFrame(NAVDAT_TIS_qamMode_t Mode, qamBuffer_t *Data, NAVDAT_RawFrame_t *Frame)
{
  symMultiplexCfg_t cfg;
  cfg.symSize = Mode;
  cfg.Buf.P = (UInt8_t*)Data->P;
  cfg.Buf.Size = Data->Size;

  symMultiplex_t mux;
  init_symMultiplex(&mux, &cfg);

  Frame->tisMode = Mode;
  Frame->Count.tis = NAVDAT_tisRawCountMax(Mode);
  return sym_multiplex_read_buf(&mux, Frame->tisData, Frame->Count.tis);
}

Bool_t NAVDAT_dsBufToFrame(NAVDAT_DS_qamMode_t Mode, qamBuffer_t *Data, NAVDAT_RawFrame_t *Frame)
{
  symMultiplexCfg_t cfg;
  cfg.symSize = Mode;
  cfg.Buf.P = (UInt8_t*)Data->P;
  cfg.Buf.Size = Data->Size;

  symMultiplex_t mux;
  init_symMultiplex(&mux, &cfg);

  Frame->dsMode = Mode;
  Frame->Count.ds = NAVDAT_dsRawCountMax(Mode);
  return sym_multiplex_read_buf(&mux, Frame->dsData, Frame->Count.ds);
}
