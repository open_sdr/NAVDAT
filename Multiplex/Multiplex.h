/*!
 * \file Multiplex.h
 * \author Шиганцов А.А.
 * \brief Заголовочный файл мультиплекса данных NAVDAT
 */

#ifndef __NAVDAT_MULTIPLEX__
#define __NAVDAT_MULTIPLEX__

#include "common.h"
#include "SDR/BASE/Multiplex/symMultiplex.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
  NAVDAT_TIS_qamMode_t modeTIS;
  NAVDAT_DS_qamMode_t modeDS;
  NAVDAT_xIS_Code_t codeXIS;
  NAVDAT_DS_Code_t codeDS;
} NAVDAT_MultiplexCfg_t;

typedef struct
{
  NAVDAT_TIS_qamMode_t modeTIS;
  NAVDAT_DS_qamMode_t modeDS;
  NAVDAT_xIS_Code_t codeXIS;
  NAVDAT_DS_Code_t codeDS;

  char SenderID[255+1];
  NAVDAT_Date_t Date;
  NAVDAT_Time_t Time;

  symMultiplex_t symMux;
} NAVDAT_Multiplex_t;

void NAVDAT_Multiplex_setSenderID(NAVDAT_Multiplex_t* This, char* str);

INLINE void NAVDAT_Multiplex_setDate(NAVDAT_Multiplex_t* This, UInt8_t year, UInt8_t mounth, UInt8_t day)
{
  This->Date.year = year;
  This->Date.mounth = mounth;
  This->Date.day = day;
}

INLINE void NAVDAT_Multiplex_setTime(NAVDAT_Multiplex_t* This, UInt8_t hour, UInt8_t min, UInt8_t sec)
{
  This->Time.hour = hour;
  This->Time.minute = min;
  This->Time.sec = sec;
}

INLINE Size_t NAVDAT_Multiplex_tisCountMax(NAVDAT_Multiplex_t* This){return NAVDAT_tisCountMax(This->modeTIS, This->codeXIS);}
INLINE Size_t NAVDAT_Multiplex_dsCountMax(NAVDAT_Multiplex_t* This){return NAVDAT_dsCountMax(This->modeDS, This->codeDS);}
INLINE Size_t NAVDAT_Multiplex_dsDataCountMax(NAVDAT_Multiplex_t* This){return NAVDAT_Multiplex_dsCountMax(This) - NAVDAT_PACKET_HEAD_SIZE;}

INLINE NAVDAT_TIS_qamMode_t NAVDAT_Multiplex_tisMode(NAVDAT_Multiplex_t* This){return This->modeTIS;}
INLINE NAVDAT_DS_qamMode_t NAVDAT_Multiplex_dsMode(NAVDAT_Multiplex_t* This){return This->modeDS;}
INLINE NAVDAT_xIS_Code_t NAVDAT_Multiplex_xisCode(NAVDAT_Multiplex_t* This){return This->codeXIS;}
INLINE NAVDAT_DS_Code_t NAVDAT_Multiplex_dsCode(NAVDAT_Multiplex_t* This){return This->codeDS;}

void init_NAVDAT_Multiplex(NAVDAT_Multiplex_t* This, NAVDAT_MultiplexCfg_t* Cfg);
Bool_t navdat_multiplex_write(NAVDAT_Multiplex_t* This, NAVDAT_Frame_t* Frame, NAVDAT_RawFrame_t* rawFrame);

void NAVDAT_Multiplex_reset(NAVDAT_Multiplex_t* This);
Bool_t navdat_multiplex_read_mis(NAVDAT_Multiplex_t* This, NAVDAT_RawFrame_t* rawFrame);
Bool_t navdat_multiplex_read_tis(NAVDAT_Multiplex_t* This, NAVDAT_RawFrame_t* rawFrame, NAVDAT_Frame_t* Frame);
Bool_t navdat_multiplex_read_ds(NAVDAT_Multiplex_t* This, NAVDAT_RawFrame_t* rawFrame, NAVDAT_Frame_t* Frame);

#ifdef __cplusplus
}
#endif

#endif // __NAVDAT_MULTIPLEX__
