/*!
 * \file Multiplex.c
 * \author Шиганцов А.А.
 * \brief Мультиплекс данных NAVDAT
 */

#include "Multiplex.h"
#include "../Modem/dataCount.h"

#include "string.h"

void NAVDAT_Multiplex_setSenderID(NAVDAT_Multiplex_t* This, char* str)
{
  strcpy(This->SenderID, str);
}

void init_NAVDAT_Multiplex(NAVDAT_Multiplex_t *This, NAVDAT_MultiplexCfg_t *Cfg)
{
  This->modeTIS = Cfg->modeTIS;
  This->modeDS = Cfg->modeDS;
  This->codeXIS = Cfg->codeXIS;
  This->codeDS = Cfg->codeDS;

  This->SenderID[0] = 0;
  This->Date.day = This->Date.mounth = This->Date.year = 0;
  This->Time.hour = This->Time.minute = This->Time.sec = 0;

  symMultiplex_reset(&This->symMux);
}

static Bool_t write_MIS(NAVDAT_Multiplex_t *This, NAVDAT_RawFrame_t* rawFrame)
{
  symMultiplexCfg_t cfgMux;
  cfgMux.symSize = 8;
  cfgMux.Buf.P = rawFrame->misData; cfgMux.Buf.Size = NAVDAT_MIS_BYTES_COUNT;
  init_symMultiplex(&This->symMux, &cfgMux);

  Bool_t res = 1;
  UInt8_t x;

  switch (This->modeTIS)
  {
  default:
  case NAVDAT_TIS_qam4:
    x = 0;
    break;

  case NAVDAT_TIS_qam16:
    x = 1;
    break;
  }
  res &= sym_multiplex_write_uint8(&This->symMux, x, 1);

  switch (This->modeDS)
  {
  default:
  case NAVDAT_DS_qam4:
    x = 0;
    break;

  case NAVDAT_DS_qam16:
    x = 1;
    break;

  case NAVDAT_DS_qam64:
    x = 2;
    break;
  }
  res &= sym_multiplex_write_uint8(&This->symMux, x, 2);

  switch(This->codeDS)
  {
  default:
  case NAVDAT_DS_Code_None:
    x = 3;
    break;

  case NAVDAT_DS_Code_Min:
    x = 0;
    break;

  case NAVDAT_DS_Code_Normal:
    x = 1;
    break;

  case NAVDAT_DS_Code_Max:
    x = 2;
    break;
  }
  res &= sym_multiplex_write_uint8(&This->symMux, x, 2);

  rawFrame->Count.mis = symMultiplex_Count(&This->symMux);
  return res;
}

static Bool_t write_TIS(NAVDAT_Multiplex_t *This, NAVDAT_Frame_t* Frame, NAVDAT_RawFrame_t* rawFrame)
{
  symMultiplexCfg_t cfgMux;
  cfgMux.symSize = 8;
  cfgMux.Buf.P = rawFrame->tisData; cfgMux.Buf.Size = NAVDAT_Multiplex_tisCountMax(This);
  init_symMultiplex(&This->symMux, &cfgMux);

  Bool_t res = 1;

  res &= sym_multiplex_write_uint8(&This->symMux, Frame->idFile, 8);
  res &= sym_multiplex_write_uint16(&This->symMux, Frame->id, 16);
  res &= sym_multiplex_write_uint8(&This->symMux, Frame->flags, 2);

  if (NAVDAT_frameIsFirst(Frame))
  {    
    Size_t n = strlen(Frame->FileName);
    res &= sym_multiplex_write_uint8(&This->symMux, n, 8);
    sym_multiplex_write_str(&This->symMux, Frame->FileName, n);

    res &= sym_multiplex_write_uint8(&This->symMux, Frame->mmsi != 0, 1);
    if (Frame->mmsi)
      sym_multiplex_write_uint32(&This->symMux, Frame->mmsi, 30);
  }
  else if (This->modeTIS != NAVDAT_TIS_qam4)
  {
    Size_t n = strlen(This->SenderID);
    res &= sym_multiplex_write_uint8(&This->symMux, n, 8);
    res &= sym_multiplex_write_str(&This->symMux, This->SenderID, n);

    res &= sym_multiplex_write_uint8(&This->symMux, This->Date.year, 7);
    res &= sym_multiplex_write_uint8(&This->symMux, This->Date.mounth, 4);
    res &= sym_multiplex_write_uint8(&This->symMux, This->Date.day, 5);
    res &= sym_multiplex_write_uint8(&This->symMux, This->Time.hour, 5);
    res &= sym_multiplex_write_uint8(&This->symMux, This->Time.minute, 6);
    res &= sym_multiplex_write_uint8(&This->symMux, This->Time.sec, 6);
  }

  rawFrame->Count.tis = symMultiplex_Count(&This->symMux);
  return res;
}

static Bool_t write_DS(NAVDAT_Multiplex_t *This, NAVDAT_Frame_t* Frame, NAVDAT_RawFrame_t* rawFrame)
{
  SDR_ASSERT(Frame->data.Size <= NAVDAT_DS64_BYTES_COUNT);

  symMultiplexCfg_t cfgMux;
  cfgMux.symSize = 8;
  cfgMux.Buf.P = rawFrame->dsData; cfgMux.Buf.Size = NAVDAT_Multiplex_dsCountMax(This);
  init_symMultiplex(&This->symMux, &cfgMux);

  Bool_t res = 1;

  res &= sym_multiplex_write_uint16(&This->symMux, Frame->timestamp, 16);

  res &= sym_multiplex_write_uint16(&This->symMux, Frame->data.Size, 16);
  res &= sym_multiplex_write_buf(&This->symMux, Frame->data.P, Frame->data.Size);

  rawFrame->Count.ds = symMultiplex_Count(&This->symMux);
  return res;
}

Bool_t navdat_multiplex_write(NAVDAT_Multiplex_t *This, NAVDAT_Frame_t* Frame, NAVDAT_RawFrame_t *rawFrame)
{
  Bool_t res = 1;

  NAVDAT_DataCounter_reset(&rawFrame->Count);

  res &= write_MIS(This, rawFrame);
  res &= write_TIS(This, Frame, rawFrame);
  res &= write_DS(This, Frame, rawFrame);

  rawFrame->tisMode = This->modeTIS;
  rawFrame->dsMode = This->modeDS;

  return res;
}

Bool_t navdat_multiplex_read_mis(NAVDAT_Multiplex_t *This, NAVDAT_RawFrame_t *rawFrame)
{
  symMultiplexCfg_t cfgMux;
  cfgMux.symSize = 8;
  cfgMux.Buf.P = rawFrame->misData; cfgMux.Buf.Size = NAVDAT_MIS_BYTES_COUNT;
  init_symMultiplex(&This->symMux, &cfgMux);

  Bool_t res = 1;
  UInt8_t x;

  res &= sym_multiplex_read_uint8(&This->symMux, &x, 1);
  switch (x)
  {
  default:
    This->modeTIS = -1;
    break;

  case 0:
    This->modeTIS = NAVDAT_TIS_qam4;
    break;

  case 1:
    This->modeTIS = NAVDAT_TIS_qam16;
    break;
  }

  res &= sym_multiplex_read_uint8(&This->symMux, &x, 2);
  switch (x)
  {
  default:
    This->modeDS = -1;
    break;

  case 0:
    This->modeDS = NAVDAT_DS_qam4;
    break;

  case 1:
    This->modeDS = NAVDAT_DS_qam16;
    break;

  case 2:
    This->modeDS = NAVDAT_DS_qam64;
    break;
  }

  res &= sym_multiplex_read_uint8(&This->symMux, &x, 2);
  switch(x)
  {
  default:
    This->codeDS = -1;

  case 0:
    This->codeDS = NAVDAT_DS_Code_Min;
    break;

  case 1:
    This->codeDS = NAVDAT_DS_Code_Normal;
    break;

  case 2:
    This->codeDS = NAVDAT_DS_Code_Max;
    break;

  case 3:
    This->codeDS = NAVDAT_DS_Code_None;
    break;
  }

  return res;
}

Bool_t navdat_multiplex_read_tis(NAVDAT_Multiplex_t *This, NAVDAT_RawFrame_t *rawFrame, NAVDAT_Frame_t *Frame)
{
  symMultiplexCfg_t cfgMux;
  cfgMux.symSize = 8;
  cfgMux.Buf.P = rawFrame->tisData; cfgMux.Buf.Size = NAVDAT_Multiplex_tisCountMax(This);
  init_symMultiplex(&This->symMux, &cfgMux);

  Bool_t res = 1;
  UInt8_t x;

  res &= sym_multiplex_read_uint8(&This->symMux, &Frame->idFile, 8);
  res &= sym_multiplex_read_uint16(&This->symMux, &Frame->id, 16);
  res &= sym_multiplex_read_uint8(&This->symMux, &Frame->flags, 2);

  Frame->mmsi = 0;
  if (NAVDAT_frameIsFirst(Frame))
  {
    UInt8_t x8 = 0;
    res &= sym_multiplex_read_uint8(&This->symMux, &x8, 8);
    sym_multiplex_read_str(&This->symMux, Frame->FileName, x8);

    res &= sym_multiplex_read_uint8(&This->symMux, &x8, 1);
    if (x8)
      sym_multiplex_read_uint32(&This->symMux, &Frame->mmsi, 30);
  }
  else if (This->modeTIS != NAVDAT_TIS_qam4)
  {
    res &= sym_multiplex_read_uint8(&This->symMux, &x, 8);
    res &= sym_multiplex_read_str(&This->symMux, This->SenderID, x);

    res &= sym_multiplex_read_uint8(&This->symMux, &This->Date.year, 7);
    res &= sym_multiplex_read_uint8(&This->symMux, &This->Date.mounth, 4);
    res &= sym_multiplex_read_uint8(&This->symMux, &This->Date.day, 5);
    res &= sym_multiplex_read_uint8(&This->symMux, &This->Time.hour, 5);
    res &= sym_multiplex_read_uint8(&This->symMux, &This->Time.minute, 6);
    res &= sym_multiplex_read_uint8(&This->symMux, &This->Time.sec, 6);
  }

  return res;
}

Bool_t navdat_multiplex_read_ds(NAVDAT_Multiplex_t *This, NAVDAT_RawFrame_t *rawFrame, NAVDAT_Frame_t *Frame)
{
  symMultiplexCfg_t cfgMux;
  cfgMux.symSize = 8;
  cfgMux.Buf.P = rawFrame->dsData; cfgMux.Buf.Size = NAVDAT_Multiplex_dsCountMax(This);
  init_symMultiplex(&This->symMux, &cfgMux);

  Bool_t res = 1;
  UInt16_t x16 = 0;

  res &= sym_multiplex_read_uint16(&This->symMux, &Frame->timestamp, 16);

  res &= sym_multiplex_read_uint16(&This->symMux, &x16, 16); Frame->data.Size = x16;
  if (Frame->data.Size <= NAVDAT_Multiplex_dsDataCountMax(This))
    res &= sym_multiplex_read_buf(&This->symMux, Frame->data.P, Frame->data.Size);
  else res = 0;

  return res;
}

void NAVDAT_Multiplex_reset(NAVDAT_Multiplex_t *This)
{
  This->modeTIS = This->modeDS = 0;
  This->codeXIS = This->codeDS = 0;
  This->SenderID[0] = 0;
  This->Date.day = This->Date.mounth = This->Date.year = 0;
  This->Time.hour = This->Time.minute = This->Time.sec = 0;
  symMultiplex_reset(&This->symMux);
}
